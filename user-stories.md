Bitwise Operations Tests Generator

	highPriority
	{
	    Title: Bitwise Operations
	    Description: Algorithms to make sure JS evaluates bitwise operations the same way C does
	
		Title: Task Generation
		Description: Algorithm to generate tasks

		Title: Task Answers Generation
		Description: Algorithm to solve generated tasks

		Title: HTML Tests Generation
		Description: Generated tasks and answers are outputted in html format (can be opened via browser)

		Title: PDF Tests Generation
		Description: Generated tasks and answers are outputted in pdf format (easy to print format)
	}

	mediumPriority
	{
		Title: ZIP Distribution
		Description: The generated tests (both html and pdf) are distributed as a zip file

		Title: Easy Interface
		Description: Provide a simple UI so that the client can generate tests fast and easy

		Title: Optimize PDF Generation
		Description: Algorithm to make pdf generation faster (without using image as template)

		Title: Optimize HTML Generation
		Description: Algorithm to make html generation faster using templates
	}

	lowPriority
	{
		Title: Test Difficulty
		Description: Algorithm to make test tasks easier using patterns
	}