function getAnswer(task, vars) {
    switch (task) {
        case 1:
            return 'a=' + type1(vars).toString(16).toUpperCase().padLeft(true, 8, '0', true);
        case 2:
            return 'res=' + type2(vars).toString(16).toUpperCase().padLeft(true, 8, '0', true);
        case 3:
            return 'shift=' + type3(vars).toString(16).toUpperCase().padLeft(true, 8, '0', true);
        case 4:
            return 'res=' + type4(vars).toString(16).toUpperCase().padLeft(true, 8, '0', true);
        case 5:
            return 'res=' + type5(vars).toString(16).toUpperCase().padLeft(true, 8, '0', true);
        case 6:
            return 'a=' + type6(vars).toString();
        case 7:
            return 'a=' + type7(vars).join(', res=');
    }
}

function type1(vars) {
    var orig = parseInt(vars[0], 16),
        insert = parseInt(vars[1], 16),
        a;

    switch (vars[2]) {
        case '&':
            return a = orig & (insert << vars[3]);
        case '^':
            return a = orig ^ (insert << vars[3]);
        case '|':
            return a = orig | (insert << vars[3]);
    }
}

function type2(vars) {
    var orig = parseInt(vars[0], 16),
        insert = parseInt(vars[1], 16),
        a, b, res;

    switch (vars[2]) {
        case '&':
            a = orig & (insert << vars[3]);
            break;
        case '^':
            a = orig ^ (insert << vars[3]);
            break;
        case '|':
            a = orig | (insert << vars[3]);
            break;
    }

    switch (vars[4]) {
        case '&':
            b = orig & (insert << vars[5]);
            break;
        case '^':
            b = orig ^ (insert << vars[5]);
            break;
        case '|':
            b = orig | (insert << vars[5]);
            break;
    }

    switch (vars[6]) {
        case '&':
            return res = a & b;
        case '^':
            return res = a ^ b;
        case '|':
            return res = a | b;
    }
}

function type3(vars) {
    var i = parseInt(vars[0], 16),
        shift;

    switch (vars[1]) {
        case '&':
            return shift = i & (1 << vars[2]);
        case '^':
            return shift = i ^ (1 << vars[2]);
        case '|':
            return shift = i | (1 << vars[2]);
    }
}

function type4(vars) {
    var value1 = new Long(parseInt(vars[0], 16)),
        value2 = new Long(parseInt(vars[1], 16)),
        res;

    value1 = value1.shiftLeft(vars[2]);
    value2 = value2.shiftRightUnsigned(vars[4]);

    switch (vars[3]) {
        case '&':
            return res = (value1.and(value2)).toInt() >>> 0;
        case '^':
            return res = (value1.xor(value2)).toInt() >>> 0;
        case '|':
            return res = (value1.or(value2)).toInt() >>> 0;
    }
}

function type5(vars) {
    var value1 = Long.fromNumber(vars[0], true),
        value2 = Long.fromNumber(vars[1], true),
        res;

    value1 = value1.shiftLeft(vars[2]);
    value2 = value2.shiftRightUnsigned(vars[4]);

    switch (vars[3]) {
        case '&':
            return res = (value1.and(value2)).toInt() >>> 0;
        case '^':
            return res = (value1.xor(value2)).toInt() >>> 0;
        case '|':
            return res = (value1.or(value2)).toInt() >>> 0;
    }
}

function type6(vars) {
    var testValue = new Long(parseInt(vars[0], 16)),
        mask = Long.fromNumber(1 << vars[2], true),
        a = 0,
        condition = 0;

    switch (vars[1]) {
        case '&':
            condition = testValue.and(mask);
            break;
        case '^':
            condition = testValue.xor(mask);
            break;
        case '|':
            condition = testValue.or(mask);
            break;
    }

    if (!condition.isZero()) {
        return a = 1;
    } else {
        return a = 2;
    }
}

function type7(vars) {
    var testValue = new Long(parseInt(vars[0], 16)),
        mask = Long.fromNumber(1 << vars[2], true),
        a = 0,
        condition,
        andFlag = false;

    if (vars[1] === '&') {
        mask = testValue.and(mask);
        andFlag = true;
    }

    condition = testValue.and(testValue);
    condition = condition.xor(andFlag ? mask : testValue);

    if (!andFlag) {
        if (vars[1] === '^') {
            condition = condition.xor(mask);
        } else if (vars[1] === '|') {
            condition = condition.or(mask);
        }
    }

    if (!condition.isZero()) {
        return [a = 1, (condition.toInt() >>> 0).toString(16).toUpperCase().padLeft(true, 8, '0', true)];
    } else {
        return [a = 2, (condition.toInt() >>> 0).toString(16).toUpperCase().padLeft(true, 8, '0', true)];
    }
}