var Long = dcodeIO.Long;

$(document).ready(function () {
//    var imgData = document.getElementById('pdf-template');

    $('button#generator').on('click', function () {
        var tests = getTests($('input.tests').val() || 5, $('input.diff').val()) || 0,
            zip = new JSZip(),
            i;

        var testTemplateSource = $('#test-template').html(),
            testTemplate = Handlebars.compile(testTemplateSource),
            testAnswersTemplateSource = $('#test-template-answers').html(),
            testAnswersTemplate = Handlebars.compile(testAnswersTemplateSource);

        var testsFolder = zip.folder('tests-html'),
            answersFolder = zip.folder('tests-html-answers'),
            pdfFolder = zip.folder('tests-pdf'),
            pdfAnswersFolder = zip.folder('tests-pdf-answers');

        for (i = 0; i < tests.length; i += 1) {
            var fileName = 'test' + (i + 1).toString().padLeft(false, tests.length.toString().length),
                testHTML = testTemplateStart + testTemplate(tests[i]) + testTemplateEnd,
                testAnswersHTML = testTemplateStart + testAnswersTemplate(tests[i]) + testTemplateEnd;

            testsFolder.file(fileName + '.html', testHTML);
            answersFolder.file(fileName + '.html', testAnswersHTML);

            var pdfTest = new jsPDF('l', 'pt'),
                pdfTestAnswers = new jsPDF('l', 'pt');

            htmlTestToPdfTest(tests[i], pdfTest);
            htmlTestToPdfTest(tests[i], pdfTestAnswers, true);

            var pdfTestData = pdfTest.output('arraybuffer'),
                pdfTestAnswersData = pdfTestAnswers.output('arraybuffer');

            pdfFolder.file(fileName + '.pdf', pdfTestData, {base64: true});
            pdfAnswersFolder.file(fileName + '.pdf', pdfTestAnswersData, {base64: true});
        }
        saveAs(zip.generate({type: "blob"}), "tests.zip");
    });

    Handlebars.registerHelper("showTag", function (index_count, block) {
        if (parseInt(index_count) % 4 === 3) {
            return block.fn(this);
        }
    });
});