function getTests(count, chance) {
    var TASKS_COUNT = 12,
        TYPES = [
            2,
            3,
            1,
            1,
            3,
            1,
            1
        ]; // how many tasks of each type

    var tests = [],
        i;
    count = count || 1;

    for (i = 0; i < count; i += 1) {
        var newTest = {
                id: undefined,
                tests: []
            },
            j, p;

        for (j = 0; j < TYPES.length; j += 1) {
            for (p = 0; p < TYPES[j]; p += 1) {
                var vars = getVars(j + 1, chance),
                    task = {
                        q: tasksTemplates[j].format(vars),
                        a: getAnswer(j + 1, vars)
                    };

                newTest['tests'].push(task);
            }
        }
        newTest['id'] = i + 1;

        tests.push(newTest);
    }

    return tests;
}

function getVars(task, chance) {
    var intMin = 0x1000,
        intMax = 0xFFFF,
        longMin = 0xFFFF,
        longMax = 0xFFFFFFFF,
        shiftMin = 1,
        shiftMax = 12;

    switch (task) {
        case 1:
            return [
                generateRandomIntChance(intMin, intMax, chance, true),
                getRandomInt(0x1, 0xF, true),
                getRandomOperator(),
                generateRandomIntChance(shiftMin, shiftMax, chance)
            ];
        case 2:
            return [
                generateRandomIntChance(intMin, intMax, chance, true),
                getRandomInt(0x1, 0xF, true),
                getRandomOperator(),
                generateRandomIntChance(shiftMin, shiftMax, chance),
                getRandomOperator(),
                generateRandomIntChance(shiftMin, shiftMax, chance),
                getRandomOperator()
            ];
        case 3:
            return [
                generateRandomIntChance(intMin, intMax, chance, true),
                getRandomOperator(),
                generateRandomIntChance(shiftMin, shiftMax, chance)
            ];
        case 4:
            return [
                generateRandomIntChance(longMin, longMax, chance, true),
                generateRandomIntChance(longMin, longMax, chance, true),
                generateRandomIntChance(shiftMin, shiftMax, chance),
                getRandomOperator(),
                generateRandomIntChance(shiftMin, shiftMax, chance),
            ];
        case 5:
            return [
                getRandomInt(0x64, 0xFFF),
                getRandomInt(0x64, 0xFFF),
                generateRandomIntChance(shiftMin, shiftMax, chance),
                getRandomOperator(),
                generateRandomIntChance(shiftMin, shiftMax, chance),
            ];
        case 6:
            return [
                generateRandomIntChance(longMin, longMax, chance, true),
                getRandomOperator(),
                generateRandomIntChance(shiftMin, shiftMax, chance)
            ];
        case 7:
            return [
                generateRandomIntChance(longMin, longMax, chance, true),
                getRandomOperator(),
                generateRandomIntChance(shiftMin, shiftMax, chance)
            ];
    }
}

function generateRandomIntChance(min, max, chance, hex) {
    var luck = Math.ceil(Math.random() * 100);
    if (chance >= luck) {
        if (hex) {
            var pattern = getRandomInt(0x10, 0xFF).toString(16);
            pattern = min === 4096 ? pattern.repeat(2) : pattern.repeat(4);

            return pattern.toUpperCase().padLeft(true, 8);
        } else {
            var shift = getRandomInt(min, max);
            while (shift % 4 !== 0) {
                shift = getRandomInt(min, max);
            }

            return shift;
        }
    } else {
        if (hex) {
            return getRandomInt(min, max, true);
        } else {
            return getRandomInt(min, max);
        }
    }
}

function getRandomOperator() {
    var chance = Math.floor(Math.random() * 3) + 1;

    switch (chance) {
        case 1:
            return '&';
        case 2:
            return '^';
        case 3:
            return '|';
    }
}

function getRandomInt(min, max, hex) {
    var retVal = Math.floor(Math.random() * (max - min + 1)) + min;

    return hex ? retVal.toString(16).toUpperCase().padLeft(true, 8) : retVal;
}

String.prototype.padLeft = function (auto, len, pad, hex) {
    var s = this;
    if (auto) len = this.length < len ? this.length : len;
    pad = pad || '0';
    while (s.length < len) s = pad + s;
    return hex ? '0x' + s.toString() : s.toString();
};

String.prototype.format = function () {
    var s = this;
    for (var i = 0; i < arguments[0].length; i += 1) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[0][i]);
    }
    return s;
};

String.prototype.repeat = function (num) {
    return new Array(num + 1).join(this);
}