// TODO: fix this hardcoded crap
function htmlTestToPdfTest(test, PDF, answers) {
    var i,
        x = 25, y = 160,
        q,
        newLine = new RegExp('<br/>', 'g'),
        space = new RegExp('&nbsp', 'g'),
        xS = [162, 162, 170, 172];

    PDF.setFontSize(10);
    //PDF.addImage(imgData, 'PNG', 20, 80, 800, 425);
    PDF.text(x, y - 68, test['id'].toString());

    PDF.rect(x - 5, y - 80, 800, 425, 'stroke');
    PDF.rect(x - 5, y - 80, 800, 67, 'stroke');
    PDF.rect(x - 5, y - 80, 800, 164, 'stroke');
    PDF.rect(x - 5, y - 80, 800, 260, 'stroke');
    PDF.rect(x - 5, 147, 505, 358, 'stroke');
    PDF.rect(x - 5, 147, 333, 358, 'stroke');
    PDF.rect(x - 5, 147, 163, 358, 'stroke');

    PDF.text(x + 365, y - 60, 'ELSYS');
    PDF.text(x + 333, y - 45, 'Software Engineering');
    PDF.text(x + 342, y - 30, 'Bitwise Operations');

    for (i = 0; i < test['tests'].length; i += 1) {
        q = test['tests'][i]['q'].replace(newLine, '\n').replace(space, ' ');
        if (answers) q = q.substr(0, q.lastIndexOf('?')) + '? ' + test['tests'][i]['a'] + q.substr(q.lastIndexOf('?') + 1);
        PDF.text(x, y, q);
        x += xS[(i + 1) % 4];
        if (i % 4 === 3) {
            x = 25;
            y += 97;
        }
    }
}